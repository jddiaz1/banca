package com.upload.crud.Annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PhoneNumericValidator.class)
@Target({ ElementType.FIELD })
public @interface PhoneNumeric {
    String message() default "El numero no es valido";
  
    int length() default 2;
    
    Class<?>[] groups() default {};
    
    Class<? extends Payload>[] payload() default {};
}
