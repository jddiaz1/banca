package com.upload.crud.Annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneValidator implements ConstraintValidator<Phone, String> {

  private int length;

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    // try {
    // return value.matches("[0-9]+") && value.length() > length;
    // } catch (Exception e) {
    // // TODO: handle exception
    // return false;
    // }

    if (value.length() < 5) {
      return false;
    } else {
      return value.matches("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,3})$");
    }

  }

}